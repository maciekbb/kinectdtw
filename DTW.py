def best_dtw_distance(a, b, dist):
  MIN_LENGTH = 15

  a = a[::-1]
  b = b[::-1]

  n = len(a)
  m = len(b)

  dtw = [[[] for i in xrange(0, m + 1)] for j in xrange(0, n + 1)]

  for i in xrange(1, n + 1):
    dtw[i][0] = float("inf")

  for i in xrange(1, m + 1):
    dtw[0][i] = float("inf")

  dtw[0][0] = 0

  for i in xrange(1, n + 1):
    for j in xrange(1, m + 1):
      cost = dist(a[i - 1], b[j - 1])
      dtw[i][j] = cost + min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1])
      # print "DTW[%d][%d] = %d" % (i, j, min(dtw[i-1][j], dtw[i][j-1], dtw[i-1][j-1]))

  best_match = dtw[n][m]

  for i in xrange(n, MIN_LENGTH, -1):
    if best_match > dtw[i][m]:
      best_match = dtw[i][m]

  return best_match
