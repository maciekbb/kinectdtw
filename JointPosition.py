class JointPosition(object):
    def __init__(self, joint):
        self.name = str(joint.JointType)
        self.x = joint.Position.X
        self.y = joint.Position.Y

    def __str__(self):
        return "%s (%f, %f)" % (self.name, self.x, self.y)