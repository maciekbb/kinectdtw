import sys
import clr

import System

sys.path.append(r'C:\Program Files\Microsoft SDKs\Kinect\v1.8\Assemblies')
clr.AddReferenceToFile(r'Microsoft.Kinect.dll')

from Microsoft.Kinect import *

class KinectDataReceiver(object):
    def __init__(self, kinect):
        self.right_wrist = None
        self.skeleton_data = System.Array.CreateInstance(Skeleton, kinect.SkeletonStream.FrameSkeletonArrayLength)

        kinect.SkeletonFrameReady += self.skeleton_frame_ready
        kinect.SkeletonStream.Enable()

    def parse_skeleton_data(self):
        tracked  = None
        try:
            tracked = [sk for sk in self.skeleton_data if sk.TrackingState == SkeletonTrackingState.Tracked][0]
        except:
            pass

        if tracked:
            self.right_wrist = [joint for joint in tracked.Joints if joint.JointType == JointType.WristRight][0]


    def skeleton_frame_ready(self, sender, event):
        # print "Skeleton frame ready"
        frame = event.OpenSkeletonFrame()
        if frame:
            frame.CopySkeletonDataTo(self.skeleton_data)
            self.parse_skeleton_data()
