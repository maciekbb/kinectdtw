import sys
import clr
import os.path
from threading import Thread
import time

from DTW import best_dtw_distance

import System

sys.path.append(r'C:\Program Files\Microsoft SDKs\Kinect\v1.8\Assemblies')
clr.AddReferenceToFile(r'Microsoft.Kinect.dll')

from Microsoft.Kinect import *

from KinectReceiver import KinectDataReceiver
from JointPosition import JointPosition

import pickle

kinect = None
try:
    kinect = [kinect for kinect in KinectSensor.KinectSensors if kinect.Status == KinectStatus.Connected][0]
except:
    print "Connect kinect first"
    quit()

kdr = KinectDataReceiver(kinect)
kinect.Start()

if os.path.isfile("./data.txt"):
    file = open("./data.txt", "r")
    sequences = pickle.load(file)
    file.close()
else:
    sequences = {}

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

FETCHING_INTERVAL = 0.02
MAX_GESTURE_TIME = 1
COORDINATE_SCALE = 100

def dist_x_y(a, b):
    return (a['x']-b['x'])*(a['x']-b['x'])+(a['y']-b['y'])*(a['y']-b['y'])

def recognize():
    while True:
        for name in sequences:
            nseq = map(lambda s: { 'x' : COORDINATE_SCALE * s.x, 'y': COORDINATE_SCALE * s.y }, seq)[-int(2*MAX_GESTURE_TIME/FETCHING_INTERVAL):]
            pattern = map(lambda s: { 'x' : COORDINATE_SCALE * s.x, 'y': COORDINATE_SCALE * s.y }, sequences[name])
            d = best_dtw_distance(nseq, pattern, dist_x_y)
            #print "%s (x) - %d" % (name, d)
            #print nseq
            #print pattern
            if d < 10000:
                print "Recognized %s" % name
        time.sleep(MAX_GESTURE_TIME)

while True:
    choice = raw_input("record (a) or recognize (b)?")
    if choice == "a":
        name = raw_input("Type gesture name ")
        duration = int(raw_input("How long will it take?"))
        sequences[name] = []
        for i in range(5):
            print "."
            time.sleep(1)
        print "Start recording!"
        for i in drange(0, duration, FETCHING_INTERVAL):
            sequences[name].append(JointPosition(kdr.right_wrist))
            time.sleep(FETCHING_INTERVAL)
        print "Gesture recorded!"

        file = open("./data.txt", "w")
        pickle.dump(sequences, file)
        file.close()

        print "Gesture saved!"
    elif choice == "b":
        seq = []
        Thread(target=recognize).start()
        while True:
            seq.append(JointPosition(kdr.right_wrist))
            time.sleep(FETCHING_INTERVAL)